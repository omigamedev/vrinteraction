#pragma once

#include "GameFramework/Pawn.h"
#include "MotionControllerComponent.h"
#include "VRPawn.generated.h"

UCLASS()
class VRINTERACTION_API AVRPawn : public APawn
{
	GENERATED_BODY()

public:
    UPROPERTY() UStaticMeshComponent* HandL;
    UPROPERTY() UStaticMeshComponent* HandR;
    UPROPERTY() UMotionControllerComponent* ControllerL;
    UPROPERTY() UMotionControllerComponent* ControllerR;

	// Sets default values for this pawn's properties
	AVRPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};
